package me.SirSlender.ToAInstances;

import java.io.File;

import me.SirSlender.ToAInstances.util.Yaml;

public class Yamls {
	private static ToAInstances toa;
	
	public static Yaml getInstanceFile(String dName){
		return new Yaml(toa.getDataFolder().getAbsolutePath() + File.separator + "instances" + File.separator + dName + ".yml");
	}
	
	public static Yaml getDungeonFile(String dName){
		return new Yaml(toa.getDataFolder().getAbsolutePath() + File.separator + "instances" + File.separator + "dungeons" + File.separator + dName + ".yml");
	}

}
