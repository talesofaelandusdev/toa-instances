package me.SirSlender.ToAInstances.util;

import java.lang.reflect.Field;

/**
 * @author zml2008
 */
public final class ReflectionUtil {

    private ReflectionUtil() {
    }

    @SuppressWarnings("unchecked")
    public static <T> T getField(Object from, String name) {
        Class<?> checkClass = from.getClass();
        do {
            try {
                Field field = checkClass.getDeclaredField(name);
                field.setAccessible(true);
                return (T) field.get(from);
            } catch (NoSuchFieldException e) {
            } catch (IllegalAccessException e) {
            }
        } while (checkClass.getSuperclass() != Object.class && ((checkClass = checkClass.getSuperclass()) != null));
        return null;
    }

}