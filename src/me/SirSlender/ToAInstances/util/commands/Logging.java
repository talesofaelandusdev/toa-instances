package me.SirSlender.ToAInstances.util.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates how the affected blocks should be hinted at in the log.
 *
 * @author sk89q
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Logging {
    public enum LogMode {
        /**
         * Player position
         */
        POSITION,

        /**
         * Region selection
         */
        REGION,

        /**
         * Player orientation and region selection
         */
        ORIENTATION_REGION,

        /**
         * Either the player position or pos1, depending on the placeAtPos1 flag
         */
        PLACEMENT,

        /**
         * Log all information available
         */
        ALL
    }

    /**
     * Log mode. Can be either POSITION, REGION, ORIENTATION_REGION, PLACEMENT or ALL.
     */
    LogMode value();
}