package me.SirSlender.ToAInstances.util.commands;

public class CommandInfo {
    private final String[] aliases;
    private final Object registeredWith;
    private final String usage, desc;
    private final String[] permissions;

    public CommandInfo(String usage, String desc, String[] aliases, Object registeredWith) {
        this(usage, desc, aliases, registeredWith, null);
    }

    public CommandInfo(String usage, String desc, String[] aliases, Object registeredWith, String[] permissions) {
        this.usage = usage;
        this.desc = desc;
        this.aliases = aliases;
        this.permissions = permissions;
        this.registeredWith = registeredWith;
    }

    public String[] getAliases() {
        return aliases;
    }

    public String getName() {
        return aliases[0];
    }

    public String getUsage() {
        return usage;
    }

    public String getDesc() {
        return desc;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public Object getRegisteredWith() {
        return registeredWith;
    }
}