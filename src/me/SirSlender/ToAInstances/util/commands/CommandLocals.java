package me.SirSlender.ToAInstances.util.commands;

import java.util.HashMap;
import java.util.Map;

public class CommandLocals {
    
    private final Map<Object, Object> locals = new HashMap<Object, Object>();

    public boolean containsKey(Object key) {
        return locals.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return locals.containsValue(value);
    }

    public Object get(Object key) {
        return locals.get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> key) {
        return (T) locals.get(key);
    }

    public Object put(Object key, Object value) {
        return locals.put(key, value);
    }

}