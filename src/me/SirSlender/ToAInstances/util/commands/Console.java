package me.SirSlender.ToAInstances.util.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation indicates that a command can be used from the console.
 *
 * @author sk89q
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Console {
}