package me.SirSlender.ToAInstances;

import java.io.File;
import java.util.logging.Logger;

import me.SirSlender.ToAInstances.Commands.ParentCommand;
import me.SirSlender.ToAInstances.Dungeons.DCommands.ParentDCommand;
import me.SirSlender.ToAInstances.Dungeons.DListener;
import me.SirSlender.ToAInstances.util.commands.ChatColor;
import me.SirSlender.ToAInstances.util.commands.CommandException;
import me.SirSlender.ToAInstances.util.commands.CommandPermissionsException;
import me.SirSlender.ToAInstances.util.commands.CommandUsageException;
import me.SirSlender.ToAInstances.util.commands.CommandsManager;
import me.SirSlender.ToAInstances.util.commands.CommandsManagerRegistration;
import me.SirSlender.ToAInstances.util.commands.MissingNestedCommandException;
import me.SirSlender.ToAInstances.util.commands.WrappedCommandException;
import me.SirSlender.ToAInstances.util.database.Database;
import me.SirSlender.ToAInstances.util.database.DatabaseConfigBuilder;
import me.SirSlender.ToAInstances.util.database.DatabaseFactory;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;



public class ToAInstances extends JavaPlugin implements CommandExecutor {
	
	Logger log = Bukkit.getLogger();
	private FileConfiguration config;
	private static String prefix = "" + ChatColor.WHITE + "[" + ChatColor.GOLD + "Instances" + ChatColor.WHITE + "] ";
	private CommandsManager<CommandSender> commands;
	private static Location loc1 = null;
	private static Location loc2 = null;
	
	@Override
	public void onEnable(){
		try{
			setupCommands();
			getServer().getPluginManager().registerEvents(new EventListener(this), this);
			getServer().getPluginManager().registerEvents(new DListener(this), this);
			DatabaseFactory factory = new DatabaseFactory(this);
			factory.generateConfigSection();
			ConfigurationSection section = getConfig().getConfigurationSection("database");
			File sqliteFile = new File(getDataFolder(), "database.db");
			DatabaseConfigBuilder config = new DatabaseConfigBuilder(section, sqliteFile);
			Database database = factory.getDatabase(config);
			database.connect(); //Its time for the real work
			db = database;
			db.registerTable(Tables.D_TABLE);
			storage = config.getStorage();
			log.info(prefix + "ToAInstances successfully enabled!");
			db.close();
		} catch (Exception e) {
			log.severe(prefix + "ToAInstances could not be enabled!");
			e.printStackTrace();
			this.setEnabled(false);
		}		
	}
	
	@Override
	public void onDisable(){
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		try{
			this.commands.execute(cmd.getName(), args, sender, sender);
		} catch (CommandPermissionsException e) {
			sender.sendMessage(ChatColor.RED + "You don't have permission.");
		} catch (MissingNestedCommandException e) {
			sender.sendMessage(ChatColor.RED + e.getUsage());
		} catch (CommandUsageException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
			sender.sendMessage(ChatColor.RED + e.getUsage());
		} catch (WrappedCommandException e) {
			if (e.getCause() instanceof NumberFormatException) {
				sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
			} else {
				sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
				e.printStackTrace();
			}
		} catch (CommandException e) {
			sender.sendMessage(ChatColor.RED + e.getMessage());
		}
		
		return true;
	}
	
	public ToAInstances getPlugin()
	{
		return this;
	}
	public FileConfiguration getConfigFile(){
		return config;
	}
	public static String getPrefix()
	{
		return prefix;
	}
	public void setLoc1(Location loc)
	{
		loc1 = loc;
	}
	public void setLoc2(Location loc)
	{
		loc2 = loc;
	}
	public static Location getLoc1()
	{
		return loc1;
	}
	public static Location getLoc2()
	{
		return loc2;
	}
	private void setupCommands(){
		this.commands = new CommandsManager<CommandSender>(){
			@Override
			public boolean hasPermission(CommandSender sender, String perm){
				return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
			}
		};
		CommandsManagerRegistration cmdRegister = new CommandsManagerRegistration(this, this.commands);
		cmdRegister.register(ParentCommand.class);
		cmdRegister.register(ParentDCommand.class);
	}
	
	public static Database db;
	public static String storage;
}
