package me.SirSlender.ToAInstances;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.SirSlender.ToAInstances.Dungeons.DungeonMethods;
import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.commands.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sucy.party.Parties;
import com.sucy.party.Party;

public class EventListener implements Listener {
	private String prefix;
	private ToAInstances plugin;
	private Parties parties;
	
	public EventListener(ToAInstances plugin){
		this.plugin = plugin;
		prefix = ToAInstances.getPrefix();
	}
	
	@EventHandler
	public void onPlayerSelect(PlayerInteractEvent e)
	{
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getPlayer().hasPermission("instances.create") && e.getPlayer().getItemInHand().getType() == Material.WOOD_SPADE){
			plugin.setLoc2(e.getClickedBlock().getLocation());
			e.setCancelled(true);
			e.getPlayer().sendMessage(prefix + "Second location set!");			
		}
		else if(e.getAction() == Action.LEFT_CLICK_BLOCK && e.getPlayer().hasPermission("instances.create") && e.getPlayer().getItemInHand().getType() == Material.WOOD_SPADE){
			plugin.setLoc1(e.getClickedBlock().getLocation());
			e.setCancelled(true);
			e.getPlayer().sendMessage(prefix + "First location set!");
		}
		
	}
	
	@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
	@EventHandler
	public void playerEnterInstance(PlayerMoveEvent e) throws SQLException
	{
		Player player = e.getPlayer();
		Location loc = player.getLocation();
		String dName = DungeonMethods.getDungeonName(loc);
		if (e.getFrom
				().getBlockX() != e.getTo().getBlockX() 
				|| e.getFrom().getBlockY() != e.getTo().getBlockY() 
				|| e.getFrom().getBlockZ() != e.getTo().getBlockZ())
		{
			Cuboid c = InstanceMethods.getInstanceEntrancePortal(dName);
			if (c.contains(e.getTo())){
			if (InstanceMethods.openDInstances(dName) > 0){
				int amount = InstanceMethods.getInstanceAmount(dName);
				for (int i = 0; i < amount; i++){
						if (!DungeonMethods.isDInUse(dName, i)){
							int dID = i;
							Party p = parties.getParty(player);
							if (p.isEmpty()){
								player.teleport(DungeonMethods.getDungeonEntranceLoc(dName, i));
								player.sendMessage(ChatColor.RED + "You have ventured into the depths of "+ ChatColor.GOLD + dName + ChatColor.RED + "alone!");
							} else {
								if (p.isLeader(player)){
									player.teleport(DungeonMethods.getDungeonEntranceLoc(dName, i));
									player.sendMessage(ChatColor.YELLOW + "You and your party have ventured into the depths of "+ ChatColor.GOLD + dName + ChatColor.YELLOW + "!");
									ArrayList<String> party = p.getMembers();
									List<UUID> partyUUID = new ArrayList();
									for (int t = 0; t < party.size(); t++){
										Player member = Bukkit.getPlayer(party.get(t));
										partyUUID.add(member.getUniqueId());
										if (member != player){
											member.teleport(DungeonMethods.getDungeonEntranceLoc(dName, i));
											member.sendMessage(ChatColor.YELLOW + "You and your party have ventured into the depths of "+ ChatColor.GOLD + dName + ChatColor.YELLOW + "!");
										}
									}
									int usedI = InstanceMethods.getUsedInstances(dName);
									int newI = usedI + 1;
									InstanceMethods.setUsedInstances(dName, newI);
									DungeonMethods.setPlayers(dName, dID, partyUUID);
									DungeonMethods.setDInUse(dName, dID, true);
								} else {
									player.sendMessage(prefix + ChatColor.RED + "Only the party leader can enter the dungeon!");
								}
							}
						}
					}
				} else {
					player.sendMessage(prefix + ChatColor.RED + "This instance is currently full! Please wait for an expedition to finish.");
				}
			}
		}
	}
	
	
}
