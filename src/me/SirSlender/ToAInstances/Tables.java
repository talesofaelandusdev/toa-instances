package me.SirSlender.ToAInstances;

import me.SirSlender.ToAInstances.util.database.Table;

public class Tables {
	public static final Table D_TABLE = new Table("dungeons", "iID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
															+ "dname VARCHAR(160),"
															+ "dlevel INT,"
															+ "amount INT DEFAULT 0,"
															+ "inuse INT DEFAULT 0,"
															+ "EnterLowerX INT DEFAULT 0,"
															+ "EnterLowerY INT DEFAULT 0,"
															+ "EnterLowerZ INT DEFAULT 0,"
															+ "EnterUpperX INT DEFAULT 0,"
															+ "EnterUpperY INT DEFAULT 0,"
															+ "EnterUpperZ INT DEFAULT 0,"
															+ "EnterWorld VARCHAR(160) DEFAULT '',"
															+ "ExitLoc VARCHAR(160)");
	//public static final Table H_TABLE = new Table("halls", "key VARCHAR(160),value INT");

}
