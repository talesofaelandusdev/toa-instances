package me.SirSlender.ToAInstances;

import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;

import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.commands.ChatColor;
import me.SirSlender.ToAInstances.util.commands.Command;
import me.SirSlender.ToAInstances.util.commands.CommandContext;
import me.SirSlender.ToAInstances.util.commands.CommandException;
import me.SirSlender.ToAInstances.util.commands.CommandPermissions;
import me.SirSlender.ToAInstances.util.commands.NestedCommand;

public class Commands {
	static ToAInstances tplugin;
	static String prefix = ToAInstances.getPrefix();
	
	public static class ParentCommand{
	@Command(aliases = { "instances", "instance" }, desc = "Instances base command.", min = 0, max = -1)
	@CommandPermissions({"instances.instances"})
	@NestedCommand(Commands.class)
	public static void instances(final CommandContext args, CommandSender sender) throws CommandException{	
	}
	}
	
	@Command(aliases = { "Create", "New", "new", "create" }, desc = "Create a new Instance.", usage = " [name] - Name of new instance | [type] - Instance type | <level> - Level of dungeon instance", min = 1, max = 3)
	@CommandPermissions({"instances.create"})
	public static void create(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iType = args.getString(1);
		String iName = args.getString(0);
		if (iType.equalsIgnoreCase("dungeon") || iType.equalsIgnoreCase("d")){
			if (InstanceMethods.doesInstanceExist(iName)){
				sender.sendMessage(prefix + ChatColor.RED + "Instance already exists by that dungeon name!");
			} else {
				int iLevel = args.getInteger(2);
				InstanceMethods.newDInstance(iName, iLevel);
				sender.sendMessage(prefix + ChatColor.GREEN + "New dungeon instance is created! " + ChatColor.YELLOW + "You can now edit it with the dungeon commands!");
			} 
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "You must specify the instance type! Currently only Dungeon is supported!");
		}
	}
	
	@Command(aliases = { "Delete", "delete", "remove", "Remove"}, desc = "Delete an instance.", usage = "[name] - Name of instance to delete | [Type] - Instance type to delete", min = 1, max = 1)
	@CommandPermissions({"instances.delete"})
	public static void delete(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		if (InstanceMethods.doesInstanceExist(iName)){
			InstanceMethods.deleteInstance(iName);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Instance has been deleted, including all entries!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Instance does not exist by that name!");
		}
	}
	
	@Command(aliases = { "SetEntrancePortal", "SetEnter", "setenter", "entportal", "EntPortal"}, desc = "Set the portal entrance for an instance to the current cuboid.", usage = "[name] - Name of instance", min = 1, max = 1)
	@CommandPermissions({"instances.setportal.enter"})
	public static void entrancePortal(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		Cuboid c = new Cuboid(ToAInstances.getLoc1(), ToAInstances.getLoc2());
		if (InstanceMethods.doesInstanceExist(iName)){
			InstanceMethods.setInstanceEntrancePortal(iName, c);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Instance entrance portal has been created!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Instance does not exist by that name!");
		}
	}
	
	@SuppressWarnings("deprecation")
	@Command(aliases = { "SetExitLocation", "SetExitLoc", "ExitLoc", "exitloc", "setexitloc"}, desc = "Set the exit location for instance exit to your location.", usage = "[name] - Instance name", min = 1, max = 1)
	@CommandPermissions({"instances.setportal.exit"})
	public static void exitLoc(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		Location loc = sender.getServer().getPlayer(sender.getName()).getLocation();
		if (InstanceMethods.doesInstanceExist(iName)){
			InstanceMethods.setInstanceExitLoc(iName, loc);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Instance exit location has been set!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Instance does not exist by that name!");
		}
	}
}
