package me.SirSlender.ToAInstances;


import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.World;

import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.Yaml;
import me.SirSlender.ToAInstances.util.database.Database;
import me.SirSlender.ToAInstances.util.database.Table;

public class InstanceMethods {
	static Database db = ToAInstances.db;
	
	public static void newDInstance(String iName, int iLevel) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
		db.connect();
		db.set(Tables.D_TABLE, null, iName, iLevel, 0, 0, 0, 0, 0, 0, 0, 0, null);
		Table newD = new Table(iName, "dID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,"
									+ "dname VARCHAR(160) DEFAULT " + iName + ","
									+ "dLowerX INT DEFAULT 0,"
									+ "dLowerY INT DEFAULT 0,"
									+ "dLowerZ INT DEFAULT 0,"
									+ "dUpperX INT DEFAULT 0,"
									+ "dUpperY INT DEFAULT 0,"
									+ "dUpperZ INT DEFAULT 0,"
									+ "dExitLowerX INT DEFAULT 0,"
									+ "dExitLowerY INT DEFAULT 0,"
									+ "dExitLowerZ INT DEFAULT 0,"
									+ "dExitUpperX INT DEFAULT 0,"
									+ "dExitUpperY INT DEFAULT 0,"
									+ "dExitUpperZ INT DEFAULT 0,"
									+ "players INT DEFAULT 0,"
									+ "dWorld VARCHAR(160) DEFAULT '',"
									+ "dEntranceLoc VARCHAR(160) DEFAULT ''," 
									+ "dInUse VARCHAR(32) DEFAULT 'FALSE'");
		db.registerTable(newD);
		db.close();}
		else{
			Yaml iFile = Yamls.getInstanceFile(iName);
			iFile.add("Name", iName);
			iFile.add("Level", iLevel);
			iFile.add("Amount", 0);
			iFile.add("InUse", 0);
			iFile.add("Players", 0);
			iFile.add("EnterWorld", "unset");
			iFile.add("EntrancePortal.LowerX", 0);
			iFile.add("EntrancePortal.LowerY", 0);
			iFile.add("EntrancePortal.LowerZ", 0);
			iFile.add("EntrancePortal.UpperX", 0);
			iFile.add("EntrancePortal.UpperY", 0);
			iFile.add("EntrancePortal.UpperZ", 0);
			iFile.add("ExitLoc", "unset");
		}
		
	}
	
	public static void deleteInstance(String iName) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
		db.connect();
		Table table;
		String index;
		if (db.contains(Tables.D_TABLE, "dname", iName)) {
			table = Tables.D_TABLE;
			index = "dname";
		}
		else {
			table = null;
			index = "";
		}
		db.remove(table, index, iName);
		db.deleteTable(iName);
		db.close();}
		else{
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iFile.delete();
		}
	}
	
	public static Location getInstanceExitLoc(String iName) throws SQLException{
		Location loc;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else { 
				table = null;
				index = "";
			}
			loc = (Location) db.get(table, index, "ExitLoc", iName);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			loc = (Location) iFile.get("ExitLoc");
		}
		return loc;
	}
	
	public static void setInstanceExitLoc(String iName, Location loc) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else { 
				table = null;
				index = "";
			}
			db.update(table, index, "ExitLoc", iName, loc);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iFile.set("ExitLoc", loc);
		}
		
	}
	
	public static Cuboid getInstanceEntrancePortal(String iName) throws SQLException{
		Location loc1;
		Location loc2;
		World dWorld;
		Cuboid c;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			if (!table.equals(null)){
				dWorld = (World) db.get(table, index, "EnterWorld", iName);
				loc1 = new Location(dWorld, (double) db.get(table, index, "EnterLowerX", iName), (double) db.get(table, index, "EnterLowerY", iName), (double) db.get(table, index, "EnterLowerZ", iName));
				loc2 = new Location(dWorld, (double) db.get(table, index, "EnterUpperX", iName), (double) db.get(table, index, "EnterUpperY", iName), (double) db.get(table, index, "EnterUpperZ", iName));
				c = new Cuboid(loc1, loc2);
			} else { 
				c = null;
			}
			db.close();
		}else{
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			if (!iFile.equals(null)){
				dWorld = (World) iFile.get("EntrancePortal.World");
				loc1 = new Location(dWorld, iFile.getDouble("EntrancePortal.LowerX"), iFile.getDouble("EntrancePortal.LowerY"), iFile.getDouble("EntrancePortal.LowerZ"));
				loc2 = new Location(dWorld, iFile.getDouble("EntrancePortal.UpperX"), iFile.getDouble("EntrancePortal.UpperY"), iFile.getDouble("EntrancePortal.UpperZ"));
				c = new Cuboid(loc1, loc2);
			} else {
				c = null;
			}
		}
		return c;		
	}
	
	public static void setInstanceEntrancePortal(String iName, Cuboid c) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			db.update(table, index, "EnterLowerX", iName, c.getLowerX());
			db.update(table, index, "EnterLowerY", iName, c.getLowerY());
			db.update(table, index, "EnterLowerZ", iName, c.getLowerZ());
			db.update(table, index, "EnterUpperX", iName, c.getUpperX());
			db.update(table, index, "EnterUpperY", iName, c.getUpperY());
			db.update(table, index, "EnterUpperZ", iName, c.getUpperZ());
			db.update(table, index, "EnterWorld", iName, c.getWorld());
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iFile.set("EntrancePortal.World", c.getWorld());
			iFile.set("EntrancePortal.LowerX", c.getLowerX());
			iFile.set("EntrancePortal.LowerY", c.getLowerY());
			iFile.set("EntrancePortal.LowerZ", c.getLowerZ());
			iFile.set("EntrancePortal.UpperX", c.getUpperX());
			iFile.set("EntrancePortal.UpperY", c.getUpperY());
			iFile.set("EntrancePortal.UpperZ", c.getUpperZ());
		}
	}
	
	public static void setInstanceAmount(String iName, int amount) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			db.update(table, index, "amount", iName, amount);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iFile.set("Amount", amount);
		}
	}
	
	public static int getInstanceAmount(String iName) throws SQLException{
		int iAmount;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			iAmount = (int) db.get(table, index, "amount", iName);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iAmount = iFile.getInteger("Amount");
		}
		return iAmount;
	}
	
	public static void setUsedInstances(String iName, int am) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			db.update(table, index, "inuse", iName, am);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			iFile.set("InUse", am);
		}
	}
	
	public static int getUsedInstances(String iName) throws SQLException{
		int inUse;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			inUse = (int) db.get(table, index, "inuse", iName);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			inUse = iFile.getInteger("InUse");
		}
		return inUse;
	}
	
	public static int openDInstances(String iName) throws SQLException{
		int open = getInstanceAmount(iName) - getUsedInstances(iName);
		return open;
	}
	
	public static void renameInstance(String iName, String newName) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			db.update(table, index, index, iName, newName);
			db.close();
		} else {
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml newFile = Yamls.getInstanceFile(newName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			newFile.add("Name", iFile.get("Name"));
			newFile.add("Level", iFile.get("Level"));
			newFile.add("Amount", iFile.get("Amount"));
			newFile.add("InUse", iFile.get("InUse"));
			newFile.add("EnterWorld", iFile.get("EnterWorld"));
			newFile.add("ExitLoc", iFile.get("ExitLoc"));
			newFile.add("EntrancePortal.LowerX", iFile.get("EntrancePortal.LowerX"));
			newFile.add("EntrancePortal.LowerY", iFile.get("EntrancePortal.LowerY"));
			newFile.add("EntrancePortal.LowerZ", iFile.get("EntrancePortal.LowerZ"));
			newFile.add("EntrancePortal.UpperX", iFile.get("EntrancePortal.UpperX"));
			newFile.add("EntrancePortal.UpperY", iFile.get("EntrancePortal.UpperY"));
			newFile.add("EntrancePortal.UpperZ", iFile.get("EntrancePortal.UpperZ"));
		}
	}
	
	public static boolean doesInstanceExist(String iName) throws SQLException{
		boolean exists;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			if (db.contains(table, index, iName)){
				exists = true;
			}
			else{
				exists = false;
			}
			db.close();
		}else{
			Yaml dFile = Yamls.getInstanceFile(iName);
			Yaml iFile;
			if (dFile.getString("Name").equals(iName)) {
				iFile = dFile;
			}
			else {
				iFile = null;
			}
			if (iFile.contains(iName)) exists = true;
			else exists = false;
		}
		return exists;
	}

}
