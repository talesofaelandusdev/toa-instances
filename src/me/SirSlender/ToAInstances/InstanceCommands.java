package me.SirSlender.ToAInstances;

import org.bukkit.command.CommandSender;

import me.SirSlender.ToAInstances.util.commands.Command;
import me.SirSlender.ToAInstances.util.commands.CommandContext;
import me.SirSlender.ToAInstances.util.commands.CommandException;
import me.SirSlender.ToAInstances.util.commands.NestedCommand;

public class InstanceCommands {
	static ToAInstances tplugin;
	static String prefix = tplugin.getPrefix();
	
	public static class ParentCommand{
	@Command(aliases = { "instance", "instances" }, desc = "Instances base command.", min = 0, max = -1)
	@NestedCommand(Command.class)
	public static void instances(final CommandContext args, CommandSender sender) throws CommandException{
		
	}
	}
	
	@Command(aliases = { "New", "new", "Create", "create" }, desc = "Dungeon section of commands for Instances.", usage = "/Dungeon [option] <variables>", flags = "dg", min = 0, max = -1)
	public static void create(final CommandContext args, CommandSender sender) throws Exception{
		
	}

}
