package me.SirSlender.ToAInstances.Dungeons;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import me.SirSlender.ToAInstances.InstanceMethods;
import me.SirSlender.ToAInstances.Tables;
import me.SirSlender.ToAInstances.ToAInstances;
import me.SirSlender.ToAInstances.Yamls;
import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.Yaml;
import me.SirSlender.ToAInstances.util.database.Database;
import me.SirSlender.ToAInstances.util.database.Table;

public class DungeonMethods {
	static Database db = ToAInstances.db;
	static ToAInstances toa;
	
	public static int registerDungeon(Object t, Cuboid c) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table dTable;
			Table table = (Table) t;
			if (db.tableExists(iName))
			{
			dTable = Tables.D_TABLE;
			index = "dname";}
			else {
				dTable = null;
				index = "";
			}
			int oldAm = (int) db.get(table, index, "Amount", iName);
			int newAm = oldAm + 1;
			db.set(table, null, index, c.getLowerX(), c.getLowerY(), c.getLowerZ(), c.getUpperX(), c.getUpperY(), c.getUpperZ(), 0, 0, 0, 0, 0, 0, c.getWorld(), "", "FALSE");
			db.update(dTable, index, "Amount", iName, newAm);
			int dID = (int) db.get(table, index, "dID", iName);
			db.close();
			return dID;
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			Yaml dFile = null;
			int oldAm = iFile.getInteger("Amount");
			int newAm = oldAm + 1;
			iFile.set("Amount", newAm);
			for (int i = 1; i <= iFile.getInteger("Amount"); i++){
				int d = i - 1;
				dFile = Yamls.getDungeonFile(iName + d);
				dFile.add("ID", d);
				dFile.add("dName", iName);
				dFile.add("World", c.getWorld());
				dFile.add("InUse", false);
				dFile.add("Dungeon.LowerX", c.getLowerX());
				dFile.add("Dungeon.LowerY", c.getLowerY());
				dFile.add("Dungeon.LowerZ", c.getLowerZ());
				dFile.add("Dungeon.UpperX", c.getUpperX());
				dFile.add("Dungeon.UpperY", c.getUpperY());
				dFile.add("Dungeon.UpperZ", c.getUpperZ());
				dFile.add("Exit.LowerX", 0);
				dFile.add("Exit.LowerY", 0);
				dFile.add("Exit.LowerZ", 0);
				dFile.add("Exit.UpperX", 0);
				dFile.add("Exit.UpperY", 0);
				dFile.add("Exit.UpperZ", 0);
				dFile.add("EntranceLoc", "");
			}
			int dID = dFile.getInteger("ID");
			return dID;
		}
	}
	
	public static void unregDungeon(Object t, int dID) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table dTable;
			Table table = (Table) t;
			if (db.tableExists(iName))
			{
			dTable = Tables.D_TABLE;
			index = "dname";}
			else {
				dTable = null;
				index = "";
			}
			db.remove(table, index, dID);
			int old = (int) db.get(dTable, index, "Amount", iName);
			int newAm = old - 1;
			db.update(dTable, index, "Amount", t, newAm);
			db.close();
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			int oldAm = iFile.getInteger("Amount");
			int newAm = oldAm - 1;
			iFile.set("Amount", newAm);
			dFile.delete();
		}
	}
	
	public static boolean doesDungeonExist(Object t, int dID) throws SQLException{
		String iName = (String) t;
		boolean exists;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			if (db.contains(table, index, dID)){
				exists = true;
			} else exists = false;
			db.close();
		} else {
			File dFile = new File(toa.getDataFolder().getAbsolutePath() + File.separator + "instances" + File.separator + "dungeons" + File.separator + iName + dID + ".yml");
			if (dFile.exists()) exists = true;
			else exists = false;
		}
		return exists;
	}
	
	public static List<String> dIDS(Object t) throws SQLException {
		String iName = (String) t;
		List<String> ids = new ArrayList<String>();
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			for(int i = 0; i == (int) db.get(table, index, "dID", i); i++){
				String sI = String.valueOf(i);
				ids.add(sI);
			}
			db.close();
			return ids;
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			int am = iFile.getInteger("Amount");
			for (int i = 0; i < am; i++){
				File test = new File(toa.getDataFolder().getAbsolutePath() + File.separator + "instances" + File.separator + "dungeons" + iName + i + ".yml");
				if (test.exists()){
					ids.add(String.valueOf(i));
				}
			}
			return ids;
		}
	}
	
	public static Location getDungeonEntranceLoc(Object t, int dID) throws SQLException {
		String iName = (String) t;
		Location loc;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			loc = (Location) db.get(table, index, "dEntranceLoc", dID);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			loc = (Location) dFile.get("EntranceLoc");
		}
		return loc;
	}
	
	public static void setDungeonEntranceLoc(Object t, int dID, Location loc) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			db.update(table, index, "dEntranceLoc", dID, loc);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			dFile.set("EntranceLoc", loc);
		}
	}
	
	public static Cuboid getDungeonExitPortal(Object t, int dID) throws SQLException{
		String iName = (String) t;
		Cuboid c;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			World dWorld = (World) db.get(table, index, "dWorld", dID);
			Location loc1 = new Location(dWorld, (double) db.get(table, index, "dExitLowerX", dID), (double) db.get(table, index, "dExitLowerY", dID), (double) db.get(table, index, "dExitLowerZ", dID));
			Location loc2 = new Location(dWorld, (double) db.get(table, index, "dExitUpperX", dID), (double) db.get(table, index, "dExitUpperY", dID), (double) db.get(table, index, "dExitUpperZ", dID));
			c = new Cuboid(loc1, loc2);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			World dWorld = (World) dFile.get("World");
			Location loc1 = new Location(dWorld, dFile.getDouble("Exit.LowerX"), dFile.getDouble("Exit.LowerY"), dFile.getDouble("Exit.LowerZ"));
			Location loc2 = new Location(dWorld, dFile.getDouble("Exit.UpperX"), dFile.getDouble("Exit.UpperY"), dFile.getDouble("Exit.UpperZ"));
			c = new Cuboid(loc1, loc2);
		}
		return c;
	}
	
	public static void setDungeonExitPortal(Object t, int dID, Cuboid c) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			db.update(table, index, "dExitLowerX", dID, c.getLowerX());
			db.update(table, index, "dExitLowerY", dID, c.getLowerY());
			db.update(table, index, "dExitLowerZ", dID, c.getLowerZ());
			db.update(table, index, "dExitUpperX", dID, c.getUpperX());
			db.update(table, index, "dExitUpperY", dID, c.getUpperY());
			db.update(table, index, "dExitUpperZ", dID, c.getUpperZ());
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			dFile.set("Exit.LowerX", c.getLowerX());
			dFile.set("Exit.LowerY", c.getLowerY());
			dFile.set("Exit.LowerZ", c.getLowerZ());
			dFile.set("Exit.UpperX", c.getUpperX());
			dFile.set("Exit.UpperY", c.getUpperY());
			dFile.set("Exit.UpperZ", c.getUpperZ());
		}	
	}
	
	public static boolean isDInUse(Object t, int dID) throws SQLException{
		String iName = (String) t;
		boolean use;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			use = (boolean) db.get(table, index, "dInUse", dID);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			use = dFile.getBoolean("InUse");
		}
		return use;
	}
	
	public static void setDInUse(Object t, int dID, boolean inUse) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			db.update(table, index, "dInUse", dID, inUse);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			dFile.set("InUse", inUse);
		}
	}
	
	public static int getNPlayers(Object t, int dID) throws SQLException{
		String iName = (String) t;
		int players;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			@SuppressWarnings("unchecked")
			List<UUID> s = (ArrayList<UUID>) db.get(table, index, "players", dID);
			players = s.size();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			List<String> s = dFile.getStringList("Players");
			players = s.size();
		}
		return players;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<UUID> getPlayers(Object t, int dID) throws SQLException{
		String iName = (String) t;
		List<UUID> players = new ArrayList();
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			List<UUID> s = (ArrayList<UUID>) db.get(table, index, "players", dID);
			for(int i = 0; i < s.size(); i++){
				players.add(s.get(i));
			}
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			List<String> s = dFile.getStringList("Players");
			for(int i = 0; i < s.size(); i++){
				players.add(UUID.fromString(s.get(i)));
			}
		}
		return players;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void setPlayers(Object t, int dID, List<UUID> players) throws SQLException{
		String iName = (String) t;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			index = "dID";
			} else {
				table = null;
				index = "";
			}
			db.update(table, index, "players", dID, players);
			db.close();
		} else {
			Yaml dFile = Yamls.getDungeonFile(iName + dID);
			for (int i = 0; i < players.size(); i++){
				List<String> s = new ArrayList();
				s.add(String.valueOf(players.get(i)));
				dFile.createNewStringList("Players", s);
			}
		}
	}
	
	public static void playerLeft(Object t, int dID, Player player) throws SQLException{
		String iName = (String) t;
		UUID pUUID = player.getUniqueId();
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			if (db.tableExists(iName))
			{
			table = (Table) t;
			} else {
				table = null;
			}
			int test = (int) db.get(table, "dname", "amount", iName);
			for (int i = 0; i < test; i++){
				List<UUID> players = getPlayers(t, dID);
				if (players.contains(pUUID)){
					players.remove(pUUID);
					setPlayers(t, dID, players);
				}
			}
			db.close();
		} else {
			List<UUID> players = getPlayers(t, dID);
			if (players.contains(pUUID)){
				players.remove(pUUID);
				setPlayers(t, dID, players);
			}	
		}
	}
	
	public static boolean playerIsInDungeon(Object t, int dID, Player player) throws SQLException{
		boolean inDungeon;
		List<UUID> players = getPlayers(t, dID);
		if (players.contains(player)) inDungeon = true;
		else inDungeon = false;
		return inDungeon;
	}
	
	@SuppressWarnings("unchecked")
	public static String getDungeonName(Player player) throws SQLException{
		String dName = null;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Connection c = db.getConnection();
			DatabaseMetaData dbmd = c.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            while (rs.next()) {
            	String table = rs.getString("TABLE_NAME");
            	List<UUID> players = (List<UUID>) db.get(table, "dname", "players", table);
            	if (players.contains(player) && dName.isEmpty()){
            		dName = table;
            	}
            }
            db.close();
		} else {
		}
		return dName;
	}
	
	public static String getDungeonName(Location loc) throws SQLException{
		String dName = null;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Connection c = db.getConnection();
			DatabaseMetaData dbmd = c.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            while (rs.next()) {
            	String table = rs.getString("TABLE_NAME");
            	Cuboid e = InstanceMethods.getInstanceEntrancePortal(table);
            	if (e.contains(loc)){
            		dName = table;
            	}
            }
            if (dName.isEmpty()){
            	dName = null;
            }
		}
		return dName;
	}
	
	@SuppressWarnings("unchecked")
	public static int getDungeonID(Player player) throws SQLException{
		int dID = 0;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Connection c = db.getConnection();
			DatabaseMetaData dbmd = c.getMetaData();
            String[] types = {"TABLE"};
            ResultSet rs = dbmd.getTables(null, null, "%", types);
            while (rs.next()) {
            	String table = rs.getString("TABLE_NAME");
            	List<UUID> players = (List<UUID>) db.get(table, "dname", "players", table);
            	if (players.contains(player) && dID == 0){
            		dID = (int) db.get(table, "dname", "dID", table);
            	}
            }
            db.close();
		} else {
		}
		return dID;
	}
	
	public static int getDLevel(String iName) throws SQLException{
		int level;
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			level = (int) db.get(table, index, "dlevel", iName);
			db.close();
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			level = iFile.getInteger("Level");
		}
		return level;
	}
	
	public static void setDLevel(String iName, int level) throws SQLException{
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			Table table;
			String index;
			if (db.contains(Tables.D_TABLE, "dname", iName)) {
				table = Tables.D_TABLE;
				index = "dname";
			}
			else {
				table = null;
				index = "";
			}
			db.update(table, index, "dlevel", iName, level);
			db.close();
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			iFile.set("Level", level);
		}
	}
	

}
