package me.SirSlender.ToAInstances.Dungeons;

import java.sql.SQLException;
import java.util.List;

import org.bukkit.command.CommandSender;

import me.SirSlender.ToAInstances.InstanceMethods;
import me.SirSlender.ToAInstances.ToAInstances;
import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.commands.ChatColor;
import me.SirSlender.ToAInstances.util.commands.Command;
import me.SirSlender.ToAInstances.util.commands.CommandContext;
import me.SirSlender.ToAInstances.util.commands.CommandException;
import me.SirSlender.ToAInstances.util.commands.CommandPermissions;
import me.SirSlender.ToAInstances.util.commands.NestedCommand;

public class DCommands {
	static ToAInstances tplugin;
	static String prefix = ToAInstances.getPrefix();
	
	public static class ParentDCommand{
	@Command(aliases = { "dungeon", "dungeons" }, desc = "Dungeon editor base command.", min = 0, max = -1)
	@CommandPermissions({"instances.dungeons"})
	@NestedCommand(DCommands.class)
	public static void dungeons(final CommandContext args, CommandSender sender) throws CommandException{	
	}
	}
	
	@Command(aliases = { "Register", "Add", "add", "register" }, desc = "Register your current cuboid as a dungeon.", usage = " [name] - Name of instance", min = 1, max = 1)
	public static void register(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		if (InstanceMethods.doesInstanceExist(iName)){
			Cuboid c = new Cuboid(ToAInstances.getLoc1(), ToAInstances.getLoc2());
			int dID = DungeonMethods.registerDungeon(iName, c);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Dungeon successfully registered as ID: "+ ChatColor.GREEN + dID + "!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Instance does not exist by that name!");
		}
	}
	
	@Command(aliases = { "Unregister", "Remove", "remove", "unregister" }, desc = "Remove or Unregister a dungeon from the instance", usage = " [name] - Name of instance | [ID] - Dungeon ID of instance", min = 2, max = 2)
	public static void unreg(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		int dID = args.getInteger(1);
		if (DungeonMethods.doesDungeonExist(iName, dID)){
			DungeonMethods.unregDungeon(iName, dID);
			sender.sendMessage(prefix + ChatColor.RED + "Dungeon successfully unregistered!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Dungeon does not exist!");
		}
	}
	
	@Command(aliases = { "List", "Show", "list", "Show" }, desc = "Returns a list of Dungeon IDs that are registered to the selected dungeon.", usage = " [name] - Name of instance", min = 1, max = 1)
	public static void list(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		if (InstanceMethods.doesInstanceExist(iName)){
			List<String> dungeons = DungeonMethods.dIDS(iName);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Available dungeons for " + ChatColor.GOLD + iName);
			String d;
			for (int i = 0; i < dungeons.size(); i++){
				d = dungeons.get(i);
				sender.sendMessage(prefix + ChatColor.AQUA + i + " : " + ChatColor.GREEN + d);
			}
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Instance does not exist by that name!");
		}
	}
	
	@Command(aliases = { "SetExitPortal", "SetExPortal", "Exit", "exit", "setexportal", "setexitportal" }, desc = "Set the exit portal of a dungeon to your selected cuboid.", usage = " [name] - Name of instance | [ID] - dungeon ID", min = 2, max = 2)
	public static void exitportal(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		int dID = args.getInteger(1);
		Cuboid c = new Cuboid(ToAInstances.getLoc1(), ToAInstances.getLoc2());
		if (DungeonMethods.doesDungeonExist(iName, dID)){
			DungeonMethods.setDungeonExitPortal(iName, dID, c);
			sender.sendMessage(prefix + ChatColor.YELLOW + "Dungeon exit portal has been registered!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Dungeon does not exist!");
		}
	}
	
	@SuppressWarnings("deprecation")
	@Command(aliases = { "SetEntranceLocation", "SetEnterLocation", "SetELoc", "Enter", "seteloc", "enter", "setentrancelocation" }, desc = "Set the entrance of a dungeon to your current location.", usage = " [name] - Name of instance | [ID] - dungeon ID", min = 2, max = 2)
	public static void enterLoc(final CommandContext args, CommandSender sender) throws CommandException,SQLException{
		String iName = args.getString(0);
		int dID = args.getInteger(1);
		if (DungeonMethods.doesDungeonExist(iName, dID)){
			DungeonMethods.setDungeonEntranceLoc(iName, dID, sender.getServer().getPlayer(sender.getName()).getLocation());
			sender.sendMessage(prefix + ChatColor.YELLOW + "Dungeon entrance location has been registered!");
		} else {
			sender.sendMessage(prefix + ChatColor.RED + "Dungeon does not exist!");
		}
	}

}
