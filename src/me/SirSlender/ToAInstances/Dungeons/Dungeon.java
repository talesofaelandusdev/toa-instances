package me.SirSlender.ToAInstances.Dungeons;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import me.SirSlender.ToAInstances.InstanceMethods;
import me.SirSlender.ToAInstances.Tables;
import me.SirSlender.ToAInstances.ToAInstances;
import me.SirSlender.ToAInstances.Yamls;
import me.SirSlender.ToAInstances.util.Cuboid;
import me.SirSlender.ToAInstances.util.Yaml;
import me.SirSlender.ToAInstances.util.database.Database;
import me.SirSlender.ToAInstances.util.database.Table;

public class Dungeon
{
	private int dID;
	private Cuboid c;
	private Table t;
	private ToAInstances toa;
	public Dungeon(Table t, Cuboid c) throws SQLException
	{
		this.t = t;
		this.c = c;
		if(ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL"))
		{
		
		}
		else
		{
		
		}
	}
	private int registerDungeon(Table table, Cuboid c) throws SQLException{
		String iName = table.getName();
		if (ToAInstances.storage.equals("MYSQL") || ToAInstances.storage.equals("SQL")){
			db.connect();
			String index;
			if (db.tableExists(iName))
			{
			dTable = Tables.D_TABLE;
			index = "dname";}
			else {
				dTable = null;
				index = "";
			}
			int oldAm = (int) db.get(table, index, "Amount", iName);
			int newAm = oldAm + 1;
			db.set(table, null, index, c.getLowerX(), c.getLowerY(), c.getLowerZ(), c.getUpperX(), c.getUpperY(), c.getUpperZ(), 0, 0, 0, 0, 0, 0, c.getWorld(), "", "FALSE");
			db.update(dTable, index, "Amount", iName, newAm);
			int dID = (int) db.get(table, index, "dID", iName);
			db.close();
			return dID;
		} else {
			Yaml iFile = Yamls.getInstanceFile(iName);
			Yaml dFile = null;
			int oldAm = iFile.getInteger("Amount");
			int newAm = oldAm + 1;
			iFile.set("Amount", newAm);
			for (int i = 1; i <= iFile.getInteger("Amount"); i++){
				int d = i - 1;
				dFile = Yamls.getDungeonFile(iName + d);
				dFile.add("ID", d);
				dFile.add("dName", iName);
				dFile.add("World", c.getWorld());
				dFile.add("InUse", false);
				dFile.add("Dungeon.LowerX", c.getLowerX());
				dFile.add("Dungeon.LowerY", c.getLowerY());
				dFile.add("Dungeon.LowerZ", c.getLowerZ());
				dFile.add("Dungeon.UpperX", c.getUpperX());
				dFile.add("Dungeon.UpperY", c.getUpperY());
				dFile.add("Dungeon.UpperZ", c.getUpperZ());
				dFile.add("Exit.LowerX", 0);
				dFile.add("Exit.LowerY", 0);
				dFile.add("Exit.LowerZ", 0);
				dFile.add("Exit.UpperX", 0);
				dFile.add("Exit.UpperY", 0);
				dFile.add("Exit.UpperZ", 0);
				dFile.add("EntranceLoc", "");
			}
			int dID = dFile.getInteger("ID");
			return dID;
		}
	}
}