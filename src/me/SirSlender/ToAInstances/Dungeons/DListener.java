package me.SirSlender.ToAInstances.Dungeons;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.SirSlender.ToAInstances.InstanceMethods;
import me.SirSlender.ToAInstances.ToAInstances;
import me.SirSlender.ToAInstances.util.Cuboid;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sucy.party.Parties;
import com.sucy.party.Party;

public class DListener implements Listener {
	private Parties parties;
	
	public DListener(ToAInstances plugin){
	}

	@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
	@EventHandler
	public void onPlayerExitInstance(PlayerMoveEvent e) throws SQLException{
		Player player = e.getPlayer();
		Location loc = player.getLocation();
		String dName = DungeonMethods.getDungeonName(loc);
		if (e.getFrom().getBlockX() != e.getTo().getBlockX() 
			|| e.getFrom().getBlockY() != e.getTo().getBlockY() 
			|| e.getFrom().getBlockZ() != e.getTo().getBlockZ()){
			int amount = InstanceMethods.getInstanceAmount(dName);
			for (int i = 0; i < amount; i++){
				Cuboid c = DungeonMethods.getDungeonExitPortal(dName, i);
				if (c.contains(e.getTo())){
					int dID = i;
					Party p = parties.getParty(player);
					if (p.isEmpty()){
						int usedI = InstanceMethods.getUsedInstances(dName);
						int newI = usedI - 1;
						InstanceMethods.setUsedInstances(dName, newI);
						DungeonMethods.setDInUse(dName, dID, false);
						DungeonMethods.setPlayers(dName, dID, null);
						player.teleport(InstanceMethods.getInstanceExitLoc(dName));
					} else {
						ArrayList<String> party = p.getMembers();
						List<UUID> partyUUID = new ArrayList();
						for (int t = 0; t < party.size(); t++){
							Player member = Bukkit.getPlayer(party.get(t));
							partyUUID.add(member.getUniqueId());
						}
						List<UUID> pcheck = DungeonMethods.getPlayers(dName, dID);
						if (partyUUID.equals(pcheck)){
							DungeonMethods.playerLeft(dName, dID, player);
							player.teleport(InstanceMethods.getInstanceExitLoc(dName));
							if (DungeonMethods.getNPlayers(dName, dID) == 0){
								int usedI = InstanceMethods.getUsedInstances(dName);
								int newI = usedI - 1;
								InstanceMethods.setUsedInstances(dName, newI);
								DungeonMethods.setDInUse(dName, dID, false);
								DungeonMethods.setPlayers(dName, dID, null);
							}
						}
					}
				}
			}
		}
	}
		
		@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
		@EventHandler
		public void playerLeftServerInInstance(PlayerQuitEvent e) throws SQLException
		{
			Player player = e.getPlayer();
			Location loc = player.getLocation();
			String dName = DungeonMethods.getDungeonName(loc);
			int amount = InstanceMethods.getInstanceAmount(dName);
			for (int i = 0; i < amount; i++)
			{
				int dID = i;
				Party p = parties.getParty(player);
				if (p.isEmpty())
				{
					int usedI = InstanceMethods.getUsedInstances(dName);
					int newI = usedI - 1;
					InstanceMethods.setUsedInstances(dName, newI);
					DungeonMethods.setDInUse(dName, dID, false);
					DungeonMethods.setPlayers(dName, dID, null);
					player.teleport(InstanceMethods.getInstanceExitLoc(dName));
				} 
				else 
				{
					ArrayList<String> party = p.getMembers();
					List<UUID> partyUUID = new ArrayList();
					for (int t = 0; t < party.size(); t++)
					{
						Player member = Bukkit.getPlayer(party.get(t));
						partyUUID.add(member.getUniqueId());
					}
					List<UUID> pcheck = DungeonMethods.getPlayers(dName, dID);
					if (partyUUID.equals(pcheck))
					{
						DungeonMethods.playerLeft(dName, dID, player);
						player.teleport(InstanceMethods.getInstanceExitLoc(dName));
					}
					if (DungeonMethods.getNPlayers(dName, dID) == 0){
						int usedI = InstanceMethods.getUsedInstances(dName);
						int newI = usedI - 1;
						InstanceMethods.setUsedInstances(dName, newI);
						DungeonMethods.setDInUse(dName, dID, false);
						DungeonMethods.setPlayers(dName, dID, null);
					}
				}
			}
		}
		
		@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
		@EventHandler
		public void playerDiedInInstance(PlayerDeathEvent e) throws SQLException
		{
			Player player = e.getEntity();
			Location loc = player.getLocation();
			String dName = DungeonMethods.getDungeonName(loc);
			int amount = InstanceMethods.getInstanceAmount(dName);
			for (int i = 0; i < amount; i++)
			{
				int dID = i;
				Party p = parties.getParty(player);
				if (p.isEmpty())
				{
					int usedI = InstanceMethods.getUsedInstances(dName);
					int newI = usedI - 1;
					InstanceMethods.setUsedInstances(dName, newI);
					DungeonMethods.setDInUse(dName, dID, false);
					DungeonMethods.setPlayers(dName, dID, null);
					player.teleport(InstanceMethods.getInstanceExitLoc(dName));
				} 
				else 
				{
					ArrayList<String> party = p.getMembers();
					List<UUID> partyUUID = new ArrayList();
					for (int t = 0; t < party.size(); t++)
					{
						Player member = Bukkit.getPlayer(party.get(t));
						partyUUID.add(member.getUniqueId());
					}
					List<UUID> pcheck = DungeonMethods.getPlayers(dName, dID);
					if (partyUUID.equals(pcheck))
					{
						DungeonMethods.playerLeft(dName, dID, player);
						player.teleport(InstanceMethods.getInstanceExitLoc(dName));
					}
					if (DungeonMethods.getNPlayers(dName, dID) == 0){
						int usedI = InstanceMethods.getUsedInstances(dName);
						int newI = usedI - 1;
						InstanceMethods.setUsedInstances(dName, newI);
						DungeonMethods.setDInUse(dName, dID, false);
						DungeonMethods.setPlayers(dName, dID, null);
					}
				}
			}
		}

}
